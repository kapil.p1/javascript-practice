/*

// linking  JavaScript File
let js = "amazing";
console.log(40 + 8 + 23 - 10);

// Values and Variables

console.log("Jonas");
console.log(23);

let firstName = "Matilda";

console.log(firstName);
console.log(firstName);
console.log(firstName);

// Variable name conventions
let jonas_matilda = "JM";
let $function = 27;

let person = "jonas";
let PI = 3.1415;

let myFirstJob = "Coder";
let myCurrentJob = "Teacher";

let job1 = "programmer";
let job2 = "teacher";

console.log(myFirstJob);

// Data Types

let javascriptIsFun = true;
console.log(javascriptIsFun);

// console.log(typeof true);
console.log(typeof javascriptIsFun);
// console.log(typeof 23);
// console.log(typeof 'Jonas');

javascriptIsFun = 'YES!';
console.log(typeof javascriptIsFun);

let year;
console.log(year);
console.log(typeof year);

year = 1991;
console.log(typeof year);

console.log(typeof null);

// let, const and var

let age = 30;
age = 31;

const birthYear = 1991;

// birthYear = 1990;
// const job;

var job = 'programmer';
job = 'teacher'

lastName = 'Schmedtmann';
console.log(lastName);

// Math operators

const now = 2037;
const ageJonas = now - 1991;
const ageSarah = now - 2018;
console.log(ageJonas, ageSarah);

console.log(ageJonas * 2, ageJonas / 10, 2 ** 3);
// 2 ** 3 means 2 to the power of 3 = 2 * 2 * 2

const firstName = 'Jonas';
const lastName = 'Schmedtmann';
console.log(firstName + ' ' + lastName);

// Assignment operators
let x = 10 + 5; // 15
x += 10; // x = x + 10 = 25
x *= 4; // x = x * 4 = 100
x++; // x = x + 1
x--;
x--;
console.log(x);

// Comparison operators
console.log(ageJonas > ageSarah); // >, <, >=, <=
console.log(ageSarah >= 18);

const isFullAge = ageSarah >= 18;

console.log(now - 1991 > now - 2018);

// Operator Precedence

const now = 2037;
const ageJonas = now - 1991;
const ageSarah = now - 2018;

console.log(now - 1991 > now - 2018);

let x, y;
x = y = 25 - 10 - 5; // x = y = 10, x = 10
console.log(x, y);

const averageAge = (ageJonas + ageSarah) / 2;
console.log(ageJonas, ageSarah, averageAge);



// Coding Challenge #1

Mark and John are trying to compare their BMI (Body Mass Index), which is calculated using the formula: BMI = mass / height ** 2 = mass / (height * height). (mass in kg and height in meter).

1. Store Mark's and John's mass and height in variables
2. Calculate both their BMIs using the formula (you can even implement both versions)
3. Create a boolean variable 'markHigherBMI' containing information about whether Mark has a higher BMI than John.

TEST DATA 1: Marks weights 78 kg and is 1.69 m tall. John weights 92 kg and is 1.95 m tall.
TEST DATA 2: Marks weights 95 kg and is 1.88 m tall. John weights 85 kg and is 1.76 m tall.


*/

// const massMark = 78;
// const heightMark = 1.69;
// const massJohn = 92;
// const heightJohn = 1.95;

/*
const massMark = 95;
const heightMark = 1.88;
const massJohn = 85;
const heightJohn = 1.76;

const BMIMark = massMark / heightMark ** 2;
const BMIJohn = massJohn / (heightJohn * heightJohn);
const markHigherBMI = BMIMark > BMIJohn;

console.log(BMIMark, BMIJohn, markHigherBMI);

////////////////////////////////////
// Strings and Template Literals
const firstName = 'Jonas';
const job = 'teacher';
const birthYear = 1991;
const year = 2037;

const jonas = "I'm " + firstName + ', a ' + (year - birthYear) + ' year old ' + job + '!';
console.log(jonas);

const jonasNew = `I'm ${firstName}, a ${year - birthYear} year old ${job}!`;
console.log(jonasNew);

console.log(`Just a regular string...`);

console.log('String with \n\
multiple \n\
lines');

console.log(`String
multiple
lines`);


////////////////////////////////////
// Taking Decisions: if / else Statements
const age = 15;

if (age >= 18) {
  console.log('Sarah can start driving license car');
} else {
  const yearsLeft = 18 - age;
  console.log(`Sarah is too young. Wait another ${yearsLeft} years :)`);
}

const birthYear = 2012;

let century;
if (birthYear <= 2000) {
  century = 20;
} else {
  century = 21;
}
console.log(century);
*/

////////////////////////////////////
// Coding Challenge #2

/*
Use the BMI example from Challenge #1, and the code you already wrote, and improve it:

1. Print a nice output to the console, saying who has the higher BMI. The message can be either "Mark's BMI is higher than John's!" or "John's BMI is higher than Mark's!"
2. Use a template literal to include the BMI values in the outputs. Example: "Mark's BMI (28.3) is higher than John's (23.9)!"

HINT: Use an if/else statement

*/

/*
const massMark = 78;
const heightMark = 1.69;
const massJohn = 92;
const heightJohn = 1.95;

// const massMark = 95;
// const heightMark = 1.88;
// const massJohn = 85;
// const heightJohn = 1.76;

const BMIMark = massMark / heightMark ** 2;
const BMIJohn = massJohn / (heightJohn * heightJohn);
console.log(BMIMark, BMIJohn);

if (BMIMark > BMIJohn) {
  console.log(`Mark's BMI (${BMIMark}) is higher than John's (${BMIJohn})!`)
} else {
  console.log(`John's BMI (${BMIJohn}) is higher than Marks's (${BMIMark})!`)
}

////////////////////////////////////
// Type Conversion and Coercion

// type conversion
const inputYear = '1991';
console.log(Number(inputYear), inputYear);
console.log(Number(inputYear) + 18);

console.log(Number('Jonas'));
console.log(typeof NaN);

console.log(String(23), 23);

// type coercion
console.log('I am ' + 23 + ' years old');
console.log('23' - '10' - 3);
console.log('23' / '2');
console.log('23' > '18');

let n = '1' + 1; // '11'
n = n - 1;
console.log(n);

////////////////////////////////////
// Truthy and Falsy Values

// 5 falsy values: 0, '', undefined, null, NaN
console.log(Boolean(0));
console.log(Boolean(undefined));
console.log(Boolean('Jonas'));
console.log(Boolean({}));
console.log(Boolean(''));

const money = 100;
if (money) {
  console.log("Don't spend it all ;)");
} else {
  console.log('You should get a job!');
}

let height = 0;
if (height) {
  console.log('YAY! Height is defined');
} else {
  console.log('Height is UNDEFINED');
}

////////////////////////////////////
// Equality Operators: == vs. ===
const age = '18';
if (age === 18) console.log('You just became an adult :D (strict)');

if (age == 18) console.log('You just became an adult :D (loose)');

const favourite = Number(prompt("What's your favourite number?"));
console.log(favourite);
console.log(typeof favourite);

if (favourite === 23) { // 22 === 23 -> FALSE
  console.log('Cool! 23 is an amzaing number!')
} else if (favourite === 7) {
  console.log('7 is also a cool number')
} else if (favourite === 9) {
  console.log('9 is also a cool number')
} else {
  console.log('Number is not 23 or 7 or 9')
}

if (favourite !== 23) console.log('Why not 23?');

////////////////////////////////////
// Logical Operators
const hasDriversLicense = true; // A
const hasGoodVision = true; // B

console.log(hasDriversLicense && hasGoodVision);
console.log(hasDriversLicense || hasGoodVision);
console.log(!hasDriversLicense);

// if (hasDriversLicense && hasGoodVision) {
//   console.log('Sarah is able to drive!');
// } else {
//   console.log('Someone else should drive...');
// }

const isTired = false; // C
console.log(hasDriversLicense && hasGoodVision && isTired);

if (hasDriversLicense && hasGoodVision && !isTired) {
  console.log('Sarah is able to drive!');
} else {
  console.log('Someone else should drive...');
}
*/

////////////////////////////////////
// Coding Challenge #3

/*
There are two gymnastics teams, Dolphins and Koalas. They compete against each other 3 times. The winner with the highest average score wins the a trophy!

1. Calculate the average score for each team, using the test data below
2. Compare the team's average scores to determine the winner of the competition, and print it to the console. Don't forget that there can be a draw, so test for that as well (draw means they have the same average score).

3. BONUS 1: Include a requirement for a minimum score of 100. With this rule, a team only wins if it has a higher score than the other team, and the same time a score of at least 100 points. HINT: Use a logical operator to test for minimum score, as well as multiple else-if blocks
4. BONUS 2: Minimum score also applies to a draw! So a draw only happens when both teams have the same score and both have a score greater or equal 100 points. Otherwise, no team wins the trophy.

TEST DATA: Dolphins score 96, 108 and 89. Koalas score 88, 91 and 110
TEST DATA BONUS 1: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 123
TEST DATA BONUS 2: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 106


*/

/*
// const scoreDolphins = (96 + 108 + 89) / 3;
// const scoreKoalas = (88 + 91 + 110) / 3;
// console.log(scoreDolphins, scoreKoalas);

// if (scoreDolphins > scoreKoalas) {
//   console.log('Dolphins win the trophy');
// } else if (scoreKoalas > scoreDolphins) {
//   console.log('Koalas win the trophy');
// } else if (scoreDolphins === scoreKoalas) {
//   console.log('Both win the trophy!');
// }

// BONUS-1

const scoreDolphins = (97 + 112 + 80) / 3;
const scoreKoalas = (109 + 95 + 50) / 3;
console.log(scoreDolphins, scoreKoalas);

if (scoreDolphins > scoreKoalas && scoreDolphins >= 100) {
  console.log('Dolphins win the trophy');
} else if (scoreKoalas > scoreDolphins && scoreKoalas >= 100) {
  console.log('Koalas win the trophy');
} else if (scoreDolphins === scoreKoalas && scoreDolphins >= 100 && scoreKoalas >= 100) {
  console.log('Both win the trophy!');
} else {
  console.log('No one wins the trophy');
}

// The switch Statement

const day = 'friday';

switch (day) {
  case 'monday': // day === 'monday'
    console.log('Plan course structure');
    console.log('Go to coding meetup');
    break;
  case 'tuesday':
    console.log('Prepare theory videos');
    break;
  case 'wednesday':
  case 'thursday':
    console.log('Write code examples');
    break;
  case 'friday':
    console.log('Record videos');
    break;
  case 'saturday':
  case 'sunday':
    console.log('Enjoy the weekend :D');
    break;
  default:
    console.log('Not a valid day!');
}

if (day === 'monday') {
  console.log('Plan course structure');
  console.log('Go to coding meetup');
} else if (day === 'tuesday') {
  console.log('Prepare theory videos');
} else if (day === 'wednesday' || day === 'thursday') {
  console.log('Write code examples');
} else if (day === 'friday') {
  console.log('Record videos');
} else if (day === 'saturday' || day === 'sunday') {
  console.log('Enjoy the weekend :D');
} else {
  console.log('Not a valid day!');
}

////////////////////////////////////
// Statements and Expressions
3 + 4
1991
true && false && !false

if (23 > 10) {
  const str = '23 is bigger';
}

const me = 'Jonas';
console.log(`I'm ${2037 - 1991} years old ${me}`);


// The Conditional (Ternary) Operator
const age = 23;
// age >= 18 ? console.log('I like to drink wine drink') : console.log('I like to drink water');

const drink = age >= 18 ? 'wine drink' : 'water';
console.log(drink);

let drink2;
if (age >= 18) {
  drink2 = 'wine drink';
} else {
  drink2 = 'water';
}
console.log(drink2);

console.log(`I like to drink ${age >= 18 ? 'wine drink' : 'water'}`);
*/

/*
Coding Challenge #4

Steven wants to build a very simple tip calculator for whenever he goes eating in a resturant. In his country, it's usual to tip 15% if the bill value is between 50 and 300. If the value is different, the tip is 20%.

1. Your task is to caluclate the tip, depending on the bill value. Create a variable called 'tip' for this. It's not allowed to use an if/else statement (If it's easier for you, you can start with an if/else statement, and then try to convert it to a ternary operator!)
2. Print a string to the console containing the bill value, the tip, and the final value (bill + tip). Example: 'The bill was 275, the tip was 41.25, and the total value 316.25'

TEST DATA: Test for bill values 275, 40 and 430

HINT: To calculate 20% of a value, simply multiply it by 20/100 = 0.2
HINT: Value X is between 50 and 300, if it's >= 50 && <= 300 

*/

const bill = 430
const tip = bill <= 300 && bill >= 50 ? bill * 0.15 : bill * 0.2
console.log(
  `The bill was ${bill}, the tip was ${tip}, and the total value ${bill + tip}`
)

// Arrow function
let year
const calcAge = (birth) => 2037 - birth
const age3 = calcAge(1993)
console.log(age3)

const yearuntil = (birth, name) => {
  const age = 2021 - birth
  const retire = 65 - age
  return `${name} retires in ${retire}`
}
console.log(yearuntil(1993, 'kapil'))

// function callling in another function

function cut(fruit) {
  return fruit * 4
}
function fruitgenb(apples, oranges) {
  const applepieces = cut(apples)
  const orangepieces = cut(oranges)
  const juice = `juice with ${applepieces} apple and ${orangepieces} orange`
  return juice
}
console.log(fruitgenb(2, 3))

const cage = function (birth) {
  return 2021 - birth
}

const yruntil = function (birth, name) {
  const age = cage(birth)
  const retire = 60 - age
  if (retire > 0) {
    return retire
  } else {
    return -1
  }
}
console.log(yruntil(1993, 'kapil'))

// function declarartion
function findAge(birth) {
  return 2021 - birth
}

// function expression
const agevalue = function (birth) {
  return 2021 - birth
}

// Arrow function

const calage4 = (births) => 2021 - births

console.log(calage4(1993))

// coding challenge 1 Part - 2

// Test - 1

const calavg = (a, b, c) => (a + b + c) / 3
console.log(calavg(3, 4, 5))

const scoreDolphins = calavg(44, 23, 71)
const scoreKoalas = calavg(65, 54, 49)

const winner = function (avgDolphins, avgKoalas) {
  if (avgDolphins >= 2 * avgKoalas) {
    console.log('Dolphin wins trophy')
  } else if (avgKoalas >= 2 * avgDolphins) {
    console.log('Koalas wins trophy')
  } else {
    console.log('No one winner')
  }
}
winner(scoreDolphins, scoreKoalas)
winner(576, 111)

// Test - 2
const scoreDolphinsss = calavg(85, 54, 41)
const scoreKoalasss = calavg(23, 34, 47)
console.log(scoreDolphinsss, scoreKoalasss)
winner(scoreDolphinsss, scoreKoalasss)

// Array
const kap = ['kp', 'aj', 'sk']
console.log(kap)

console.log(kap + 10)

const newl = kap.push('pk')
console.log(kap)
console.log(newl)

kap.unshift('lp')
console.log(kap)

console.log(kap.pop())
console.log(kap)

console.log(kap.shift())
console.log(kap)

console.log(kap.indexOf('aj'))
console.log(kap.includes('aj'))

// coding 2 part -

const caltip = function (bill) {
  return bill >= 500 && bill <= 300 ? bill * 0.15 : bill * 0.2
}

const bills = [125, 555, 44]
const tips = [caltip(bills[0]), caltip(bills[1]), caltip(bills[2])]
const total = [bills[0] + tips[0], bills[1] + tips[1], bills[2] + tips[2]]
console.log(bill, tips, total)

// objects

const kapi = {
  name: 'Kapil',
  email: 'kp247700@gmail.com',
}
console.log(kapi)

// Dot Vs bracket Notation
// const age = prompt("your age");
// console.log(`your age is ${age}`);

// Object in function
const kapl = {
  name: 'Kapil',
  age: 27,
  city: 'nanded',
  calcAge: function (year) {
    return 2021 - year
  },
}
console.log(kapl.calcAge(1993))
console.log(kapl['calcAge'](1993))

// this keywords

const kapli = {
  name: 'Kapil',
  birth: 1993,
  age: 28,
  job: 'eng',
  calAge: function () {
    return 2021 - this.birth
  },
}
console.log(kapli.calAge())

// this keyword use

const pas = {
  name: 'Kapil',
  birth: 1993,
  vehicle: true,
  job: 'eng',
  calAge: function () {
    this.age = 2021 - this.birth
    return this.age
  },
  getsum: function () {
    return `${this.name} is a ${this.calAge} year old ${this.job}, and he has ${
      this.vehicle ? 'a' : 'no'
    } vehicle`
  },
}

console.log(pas.calAge())
console.log(pas.calAge())
console.log(pas.getsum())

// coding challenge part - 2

const mark = {
  name: 'Mark',
  mass: 65,
  heigth: 1.59,
  calBMI: function () {
    this.bmi = this.mass / this.heigth ** 2
    return this.bmi
  },
}
console.log(mark.calBMI())
console.log(mark.bmi)

const john = {
  name: 'John',
  mass: 78,
  heigth: 1.69,
  calBMI: function () {
    this.bmi = this.mass / this.heigth ** 2
    return this.bmi
  },
}

console.log(john.calBMI())
console.log(john.bmi)

if (mark.bmi > john.bmi) {
  console.log(`${mark.name}'s bmi (${mark.bmi}) is more than ${john.name}`)
} else {
  console.log(`${john.name}'s bmi (${john.bmi}) is more than ${mark.name}`)
}

// Looping array, breaking & continue

const jonas = [
  'jonas',
  'sarah',
  2021 - 1993,
  'teacher',
  ['kap', 'raj', 'sap'],
  true,
]

const types = []

for (let i = 0; i < jonas.length; i++) {
  console.log(jonas[i], typeof jonas[i])
  types[i] = typeof jonas[i]
}
console.log(types)

// Lopping backward and looping in loop

const newarr = ['kapil', 'pandit', 2021 - 1993, 'eng', ['aj', 'kp', 'am'], true]
for (let i = newarr.length - 1; i >= 0; i--) {
  console.log(i, newarr[i])
}

for (let ex = 0; ex < 4; ex++) {
  console.log(`_____start ex ${ex}`)

  for (let rep = 1; rep < 6; rep++) {
    console.log(`ex ${ex} lifting weight repititon ${rep}`)
  }
}

// while loop

let rep = 1
while (rep <= 10) {
  console.log(`lifting weight ${rep}`)
  rep++
}

let dice = Math.trunc(Math.random() * 6) + 1
while (dice !== 6) {
  console.log(`you roled dice${dice}`)
  dice = Math.trunc(Math.random() * 6) + 1
  if (dice === 6) console.log('Loop end')
}

// coding challenge - 4 Part -2

const tipcal = function (bill) {
  return bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2
}

const billlist = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52]
const tiplist = []
const totallist = []

for (let i = 0; i < billlist.length; i++) {
  const tip = tipcal(billlist[i])
  tiplist.push(tip)
  totallist.push(tip + billlist[i])
}
console.log(billlist, totallist, tiplist)

const calavg5 = function (arr) {
  let summ = 0
  for (let i = 0; i < arr.length; i++) {
    summ = summ + arr[i]
  }
}

console.log(calavg5([2, 3, 7]))
console.log(calavg5(totallist))
console.log(calavg5(tiplist))
