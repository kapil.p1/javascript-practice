'use strict';

// /////////////////////////////////////////////////
// /////////////////////////////////////////////////
// // BANKIST APP

// // Data
// const account1 = {
//   owner: 'Jonas Schmedtmann',
//   movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
//   interestRate: 1.2, // %
//   pin: 1111,
// };

// const account2 = {
//   owner: 'Jessica Davis',
//   movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
//   interestRate: 1.5,
//   pin: 2222,
// };

// const account3 = {
//   owner: 'Steven Thomas Williams',
//   movements: [200, -200, 340, -300, -20, 50, 400, -460],
//   interestRate: 0.7,
//   pin: 3333,
// };

// const account4 = {
//   owner: 'Sarah Smith',
//   movements: [430, 1000, 700, 50, 90],
//   interestRate: 1,
//   pin: 4444,
// };

// const accounts = [account1, account2, account3, account4];

// // Elements
// const labelWelcome = document.querySelector('.welcome');
// const labelDate = document.querySelector('.date');
// const labelBalance = document.querySelector('.balance__value');
// const labelSumIn = document.querySelector('.summary__value--in');
// const labelSumOut = document.querySelector('.summary__value--out');
// const labelSumInterest = document.querySelector('.summary__value--interest');
// const labelTimer = document.querySelector('.timer');

// const containerApp = document.querySelector('.app');
// const containerMovements = document.querySelector('.movements');

// const btnLogin = document.querySelector('.login__btn');
// const btnTransfer = document.querySelector('.form__btn--transfer');
// const btnLoan = document.querySelector('.form__btn--loan');
// const btnClose = document.querySelector('.form__btn--close');
// const btnSort = document.querySelector('.btn--sort');

// const inputLoginUsername = document.querySelector('.login__input--user');
// const inputLoginPin = document.querySelector('.login__input--pin');
// const inputTransferTo = document.querySelector('.form__input--to');
// const inputTransferAmount = document.querySelector('.form__input--amount');
// const inputLoanAmount = document.querySelector('.form__input--loan-amount');
// const inputCloseUsername = document.querySelector('.form__input--user');
// const inputClosePin = document.querySelector('.form__input--pin');

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

// const currencies = new Map([
//   ['USD', 'United States dollar'],
//   ['EUR', 'Euro'],
//   ['GBP', 'Pound sterling'],
// ]);

// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

/////////////////////////////////////////////////

let array = ['a', 'b', 'c', 'd', 'e'];

// SLICE
console.log(array.slice(2));
console.log(array.slice(2, 4));
console.log(array.slice(-2));
console.log(array.slice(-1));
console.log(array.slice(1, -2));
console.log(array.slice());

// SPLICE
// console.log(array.splice(2));
console.log(array);
// console.log(array.splice(-1));
console.log(array.splice(1, 2));

//REVERSE
array = ['a', 'b', 'c', 'd', 'e'];
console.log(array.reverse());
console.log(array);

// CONCAT
const array2 = ['f', 'g', 'h', 'i', 'j'];
const letters = array.concat(array2);
console.log(letters);

// JOIN
console.log(array2.join('_'));

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

for (const m of movements) {
  if (m > 0) {
    console.log(`You deposited ${m}`);
  } else {
    console.log(`you withdrew${Math.abs(m)}`);
  }
}

for (const [i, m] of movements.entries()) {
  if (m > 0) {
    console.log(`movement ${i + 1} You deposited ${m}`);
  } else {
    console.log(`movement ${i + 1} you withdrew${Math.abs(m)}`);
  }
}

// forEach

movements.forEach(function (m, i, arr) {
  if (m > 0) {
    console.log(`movement ${i + 1} You deposited ${m}`);
  } else {
    console.log(`movement ${i + 1} you withdrew${Math.abs(m)}`);
  }
});

const currencies = new Map([
  ['USD', 'United States dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);

currencies.forEach(function (value, key, map) {
  console.log(`${key} : ${value}`);
});

// set

const cur = new Set(['USD', 'EUR', 'USD', 'GBP', 'USD']);
console.log(cur);
cur.forEach(function (value, key, map) {
  console.log(`${key}:${value}`);
});

// Map method
const ar = [5, 51, 65, 10];
const eur = 1.9;
const rs = ar.map(mp => {
  mp * eur;
});
console.log(ar);
console.log(rs);

const mv = [];
for (const m of ar) mv.push(m * eur);
console.log(mv);

// const mvd = ar.map(m, i) => {
//   (`mov ${i+1}: you ${m > 0 ? 'deposit' : 'withdrew'} ${Math.abs(m)}`);
// };
// console.log(mvd);

// Filter method

const lt = [14, 52, -26, 70, -14, 30];
const dep = lt.filter(function (m) {
  return m > 0;
});
console.log(dep);

const def = [];
for (const m of lt) if (m > 0) def.push(m);
console.log(def);

const wit = lt.filter(m => m < 0);
console.log(wit);

// reduce method
const red = [114, 22, -36, 270, 54, -230];

const bal = red.reduce(function (acc, cur, i, arr) {
  console.log(`Iteration ${i}: ${acc}`);
  return acc + cur;
}, 0);
console.log(bal);

let bal2 = 0;
for (const re of red) bal2 += re;
console.log(bal2);

// Max value

const max = red.reduce((acc, m) => {
  if (acc > m) return acc;
  else return m;
}, red[0]);
console.log(max);

// Coding challenge 2 section 11

// const clavg = function (ages) {
//   const hages = ages.map(age => {
//     (age < 2 ? 2 * age : 16 + age * 4);
//   });
//   const adults = hages.filter(age => age >= 18);
//   console.log(hages);
//   console.log(adults);

//   const avg = adults.reduce((acc, age, arr) =>
//   acc + age / arr.length, 0)
//   return avg;
// };
// clavg([114, 22, -36, 270, 54, -230]);

// const avg1 = clavg([114, 22, -36, -270, 954, -2530]);

// const avg2 = clavg([854, 292, -36, 2270, 54, -1230]);
// console.log(avg1, avg2);

// Coding challenge 2 section 11

const calcAverageHumanAge = function (ages) {
  const humanAges = ages.map(age => (age <= 2 ? 2 * age : 16 + age * 4));
  const adults = humanAges.filter(age => age >= 18);
  console.log(humanAges);
  console.log(adults);

  // const average = adults.reduce((acc, age) => acc + age, 0) / adults.length;

  const average = adults.reduce(
    (acc, age, i, arr) => acc + age / arr.length,
    0
  );

  // 2 3. (2+3)/2 = 2.5 === 2/2+3/2 = 2.5

  return average;
};
const avg1 = calcAverageHumanAge([5, 2, 4, 1, 15, 8, 3]);
const avg2 = calcAverageHumanAge([16, 6, 10, 5, 6, 1, 4]);
console.log(avg1, avg2);

// Coding challenge - 3

const calage2 = function (ages) {
  const hag = ages.map(age => (age < 2 ? 2 * age : 16 + age * 4));

  const adul = hag.filter(age => age >= 18);

  const avgage = adul.reduce((acc, age, i, arr) => acc + age / arr.length, 0);
  return avgage;
};

const callage = ages =>
  ages
    .map(age => (age <= 2 ? 2 * age : 16 + age * 4))
    .filter(age => age >= 18)
    .reduce((acc, age, i, arr) => acc + age / arr.length, 0);

const avg11 = calage2([5, 2, 4, 1, 15, 8, 3]);
const avg22 = calage2([16, 6, 10, 5, 6, 1, 4]);
console.log(avg11, avg22);

const avg111 = callage([5, 2, 4, 1, 15, 8, 3]);
const avg222 = callage([16, 6, 10, 5, 6, 1, 4]);
console.log(avg111, avg222);

// find method
const gt = [5, 2, 4, -1, 15, -8, 3];
const firstw = gt.find(mov => mov < 0);
const ft = [];
ft.push(firstw);
console.log(firstw);
console.log(ft);

const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

const accc = accounts.find(acc => acc.owner === 'Sarah Smith');
console.log(accc);

// // BANKIST APP

// // Data
// const account1 = {
//   owner: 'Jonas Schmedtmann',
//   movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
//   interestRate: 1.2, // %
//   pin: 1111,
// };

// const account2 = {
//   owner: 'Jessica Davis',
//   movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
//   interestRate: 1.5,
//   pin: 2222,
// };

// const account3 = {
//   owner: 'Steven Thomas Williams',
//   movements: [200, -200, 340, -300, -20, 50, 400, -460],
//   interestRate: 0.7,
//   pin: 3333,
// };

// const account4 = {
//   owner: 'Sarah Smith',
//   movements: [430, 1000, 700, 50, 90],
//   interestRate: 1,
//   pin: 4444,
// };

// const accounts = [account1, account2, account3, account4];

// // Elements
// const labelWelcome = document.querySelector('.welcome');
// const labelDate = document.querySelector('.date');
// const labelBalance = document.querySelector('.balance__value');
// const labelSumIn = document.querySelector('.summary__value--in');
// const labelSumOut = document.querySelector('.summary__value--out');
// const labelSumInterest = document.querySelector('.summary__value--interest');
// const labelTimer = document.querySelector('.timer');

// const containerApp = document.querySelector('.app');
// const containerMovements = document.querySelector('.movements');

// const btnLogin = document.querySelector('.login__btn');
// const btnTransfer = document.querySelector('.form__btn--transfer');
// const btnLoan = document.querySelector('.form__btn--loan');
// const btnClose = document.querySelector('.form__btn--close');
// const btnSort = document.querySelector('.btn--sort');

// const inputLoginUsername = document.querySelector('.login__input--user');
// const inputLoginPin = document.querySelector('.login__input--pin');
// const inputTransferTo = document.querySelector('.form__input--to');
// const inputTransferAmount = document.querySelector('.form__input--amount');
// const inputLoanAmount = document.querySelector('.form__input--loan-amount');
// const inputCloseUsername = document.querySelector('.form__input--user');
// const inputClosePin = document.querySelector('.form__input--pin');
