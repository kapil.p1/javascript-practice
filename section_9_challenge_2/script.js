'use strict'

const game = {
  team1: 'kap',
  team2: 'Abh',
  players: [
    ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
    ['h', 'i', 'j', 'k', 'l', 'm', 'n'],
  ],
  score: '4.0',
  scored: ['kapil', 'abhay', 'sandip', 'atif'],
  date: 'Nov 9th, 2021',
  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5,
  },
}

// 1
for (const [i, player] of game.scored.entries())
  console.log(`Goal ${i + 1}: ${player}`)

// 2
const odds = Object.values(game.odds)
let avg = 0
for (const odd of odds) avg += odd
avg /= odds.length
console.log(avg)

// 3

for (const [team, odd] of Object.entries(game.odds)) {
  console.log(odd)
  const teamStr = team === 'x' ? 'draw' : `victory ${game.team}`
  console.log(teamStr)
  console.log(`Odd of ${teamStr} ${odd}`)
}
